import jax.numpy as jnp
from jax import grad, jit, vmap
from jax import random
from jax import nn
import optax
from tqdm import tqdm
import pickle

class FFNN:
    """Represents a FFNN"""

    def __init__(self, input_size: int , hidden_size: int, 
                output_size: int, dropout_rate: float = 0.05,
                learning_rate: float = 1e-3, seed: int = 42):
        
        self.key = random.PRNGKey(seed)

        self.params = self.init_params(input_size, hidden_size, output_size, self.key)

        self.dropout_rate = dropout_rate

        self.learning_rate = learning_rate

    def init_params(self, input_size: int, hidden_size: int, output_size: int, key):
        """Initializes network params"""

        key, key1, key2, key3, key4 = random.split(key, num=5)

        initial_params = {
            'hidden': random.normal(shape=[input_size, hidden_size], key=key1),
            'output': random.normal(shape=[hidden_size, output_size], key=key2),
            'bias_l1': random.normal(shape=[hidden_size,], key=key3),
            'bias_l2': random.normal(shape=[output_size,], key=key4)
        }

        self.key = key

        return initial_params
    
    def dropout(self, x, dropout_r, key):
        """Represents dropout for hidden layer"""

        mask = random.bernoulli(key, dropout_r, x.shape)

        return x * mask
    
    def net(self, x: jnp.ndarray, params: optax.Params, is_training: bool, key=random.PRNGKey(0), dropout_rate=.95) -> jnp.ndarray:
        """Forward Prop Function"""
        x = jnp.dot(x, params['hidden']) + params['bias_l1']

        x = nn.relu(x)

        if is_training:
            x = self.dropout(x, dropout_rate, key)

        x = jnp.dot(x, params['output']) + params['bias_l2']

        x = nn.softmax(x)

        return x
    
    def accuracy(self, params, x_data, y_true):
        """Accuracy Score"""

        y_pred_probs = self.net(x_data, params, False)
        
        y_pred_labels = jnp.argmax(y_pred_probs, axis=1)
        
        correct_predictions = jnp.sum(y_pred_labels == jnp.argmax(y_true, axis=1))
        
        accuracy = correct_predictions / len(y_true)
        
        return accuracy
    
    def loss(self, params: optax.Params, batch: jnp.ndarray, labels: jnp.ndarray, is_training: bool, key=random.PRNGKey(0)) -> jnp.ndarray:
        """Loss Function"""

        y_hat = self.net(batch, params, is_training, key)

        loss_value = jnp.mean(optax.l2_loss(y_hat, labels))

        return loss_value

    def train(self, x_train, y_train, epochs=5000):
        """Trains FFNN"""

        is_training = True

        optimizer = optax.adam(self.learning_rate)

        params = self.params    

        opt_state = optimizer.init(params)

        loss_ = 1

        with tqdm(total=epochs) as pbar:
            for i in range(epochs):

                if epochs % 50 == 0:
                
                    loss_ = self.loss(params, x_train, y_train, False)

                key, key1 = random.split(self.key)

                self.key = key

                grads = grad(self.loss)(params, x_train, y_train, is_training, key1)

                updates, opt_state = optimizer.update(grads, opt_state)

                params = optax.apply_updates(params, updates)

                self.params = params

                pbar.set_postfix({"Loss": loss_})
                pbar.update(1)

        print(self.accuracy(params, x_train, y_train))

        self.save_params()

    def save_params(self):
        """Save Params to file"""

        with open('ffnn_params.pkl', 'wb') as f:
            pickle.dump(self.params, f)

    def load_params(self):
        """Load Params from file"""

        with open('ffnn_params.pkl', 'rb') as f:
            saved_params = pickle.load(f)
            self.params = saved_params