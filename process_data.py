def create_x_y():
    x_data = []
    y_data = []

    with open('iris.data', 'r') as file:
        
        file_contents = file.read()

        lines = file_contents.splitlines()

        lines = lines[:len(lines)-1]

        for line in lines:

            line_data = line.strip().split(',')

            x_data.append([float(x) for x in line_data[:-1]])

            y_data.append(line_data[-1])

    distinct_y_values = list(set(y_data))

    one_hot_encodings = {}

    for i, y_val in enumerate(distinct_y_values):

        one_hot = [0] * len(distinct_y_values)

        one_hot[i] = 1

        one_hot_encodings[y_val] = one_hot

    y_encoded = [one_hot_encodings[y] for y in y_data]

    return x_data, y_encoded