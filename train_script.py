import FFNN as fn
import process_data
import jax.numpy as jnp

x_data, y_data = process_data.create_x_y()

x = jnp.asarray(x_data)
y = jnp.asarray(y_data)

network = fn.FFNN(4, 3, 3, learning_rate = 1e-1)

network.train(x, y, epochs=1000)