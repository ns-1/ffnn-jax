# ffnn-jax

Mini Project of implementing a Feed Forward Neural Network in Jax. Tested on UCI Iris Dataset, and achieved .993% accuracy w/ 1000 Epochs, LR of 1e-1, Layer sizes of 4, 4, 3. 